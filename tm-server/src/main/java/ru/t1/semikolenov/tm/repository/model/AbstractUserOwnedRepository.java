package ru.t1.semikolenov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.semikolenov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.semikolenov.tm.comparator.CreatedComparator;
import ru.t1.semikolenov.tm.comparator.DateBeginComparator;
import ru.t1.semikolenov.tm.comparator.StatusComparator;
import ru.t1.semikolenov.tm.model.AbstractUserOwnedModel;
import ru.t1.semikolenov.tm.model.User;

import java.util.Comparator;

@Repository
@Scope("prototype")
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else if (comparator == DateBeginComparator.INSTANCE) return "status";
        else return "name";
    }

    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        model.setUser(entityManager.find(User.class, userId));
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        model.setUser(entityManager.find(User.class, userId));
        entityManager.remove(model);
    }

}
